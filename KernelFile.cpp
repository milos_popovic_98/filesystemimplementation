#include "KernelFile.h"
#include "part.h"

KernelFile::KernelFile()
{
}


KernelFile::~KernelFile()
{
	kernelFS->close(fname,mode, ind1, ind2,fileBlock, index, size);
}
char KernelFile::write(BytesCnt cnt, char* buffer) {
	if (mode == 'r') return 0;
	if (size == 0) return write1(cnt, buffer);
	else return write2(cnt, buffer);
}

char KernelFile::write1(BytesCnt cnt, char* buffer) {
	char* bitVector = new char[ClusterSize];
	char* index2 = new char[ClusterSize];
	char* block = new char[ClusterSize];
	int* t = nullptr;
	int num1 = 0, num2 = 0;
	BytesCnt i = 0, j = 0;
	int* entry1 = (int*)(&index[0]);
	kernelFS->freeBit(bitVector, entry1);
	kernelFS->partition->readCluster(*entry1, index2);
	int* entry2 = (int*)(&index2[0]);
	kernelFS->freeBit(bitVector, entry2);
	kernelFS->partition->readCluster(*entry2, block);
	while (i < cnt) {
		if (j == ClusterSize) {
			kernelFS->partition->writeCluster(*entry2, block);
			j = 0;
			num2++;
			if (num2 == numEntry) {
				kernelFS->partition->writeCluster(*entry1, index2);
				num2 = 0;
				num1++;
				if (num1 == numEntry) {
					kernelFS->partition->writeCluster(*entry2, block);
					kernelFS->partition->writeCluster(*entry1, index2);
					kernelFS->partition->writeCluster(fileBlock, index);
					delete[] block; delete[] bitVector; delete[] index2;
					size = pos;
					return 0;
				}
				entry1 = (int*)(&index[num1 * 4]);
				kernelFS->freeBit(bitVector, entry1);
				kernelFS->partition->readCluster(*entry1, index2);
			}
			entry2 = (int*)(&index2[num2 * 4]);
			kernelFS->freeBit(bitVector, entry2);
			kernelFS->partition->readCluster(*entry2, block);
		}
		block[j++] = buffer[i++];
		pos++;
	}
	kernelFS->partition->writeCluster(*entry2, block);
	kernelFS->partition->writeCluster(*entry1, index2);
	//kernelFS->partition->writeCluster(fileBlock, index);
	delete[] block; delete[] bitVector; delete[] index2;
	size = pos;
	return 1;
}
char KernelFile::write2(BytesCnt cnt, char* buffer) {
	char* index2 = new char[ClusterSize];
	char* block = new char[ClusterSize];
	char* bitVector = new char[ClusterSize];
	BytesCnt num1 = pos / (numEntry*ClusterSize);
	BytesCnt num2 = (pos % (numEntry*ClusterSize)) / ClusterSize;
	BytesCnt i = 0, j = pos % ClusterSize;
	int* entry1 = (int*)(&index[num1*4]);
	if (*entry1==0) kernelFS->freeBit(bitVector, entry1);
	kernelFS->partition->readCluster(*entry1, index2);
	int* entry2 = (int*)(&index2[num2*4]);
	if (*entry2==0) kernelFS->freeBit(bitVector, entry2);
	kernelFS->partition->readCluster(*entry2, block);
	while (i < cnt) {
		if (j == ClusterSize) {
			kernelFS->partition->writeCluster(*entry2, block);
			j = 0;
			num2++;
			if (num2 == numEntry) {
				kernelFS->partition->writeCluster(*entry1, index2);
				num2 = 0;
				num1++;
				if (num1 == numEntry ) {
					kernelFS->partition->writeCluster(*entry2, block);
					kernelFS->partition->writeCluster(*entry1, index2);
					kernelFS->partition->writeCluster(fileBlock, index);
					delete[] block; delete[] bitVector; delete[] index2;
					size = pos;
					return 0;
				}
				entry1 = (int*)(&index[num1 * 4]);
				if (*entry1==0) kernelFS->freeBit(bitVector, entry1);
				kernelFS->partition->readCluster(*entry1, index2);
			}
			entry2 = (int*)(&index2[num2 * 4]);
			if (*entry2) kernelFS->freeBit(bitVector, entry2);
			kernelFS->partition->readCluster(*entry2, block);
		}
		block[j++] = buffer[i++];
		pos++;
		
	}
	kernelFS->partition->writeCluster(*entry2, block);
	kernelFS->partition->writeCluster(*entry1, index2);
	//kernelFS->partition->writeCluster(fileBlock, index);
	delete[] block; delete[] bitVector; delete[] index2;
	size = pos;
	return 1;
}
BytesCnt KernelFile::MAX = numEntry * numEntry*ClusterSize;
int KernelFile::numEntry = ClusterSize / 4;
BytesCnt KernelFile::read(BytesCnt cnt, char* buffer) {
	if (pos == size) return 0;
	BytesCnt i = 0, j = 0;
	char* index2 = new char[ClusterSize];
	char* block = new char[ClusterSize];
	BytesCnt num1 = pos / (numEntry*ClusterSize);
	BytesCnt num2 = (pos % (numEntry*ClusterSize)) / ClusterSize;
	int *entry1 = (int*)(&index[num1 * 4]);
	kernelFS->partition->readCluster(*entry1, index2);
	int* entry2 = (int*)(&index2[num2 * 4]);   
	kernelFS->partition->readCluster(*entry2, block);
	j = pos % ClusterSize;
	while (i < cnt && pos < size) {
		if (j == ClusterSize) {
			j = 0;
			num2++;
			if (num2 == numEntry) {
				num2 = 0;
				num1++;
				entry1 = (int*)(&index[num1 * 4]);
				kernelFS->partition->readCluster(*entry1, index2);
			}
			entry2 = (int*)(&index2[num2 * 4]);
			kernelFS->partition->readCluster(*entry2, block);
		}
		buffer[i++] = block[j++];
		pos++;

	}
	delete[] index2; delete[] block;
	return i;
}
char KernelFile::seek(BytesCnt p) {
	if (p >= size) return 0;
	else pos = p;
	return 1;
}
BytesCnt KernelFile::filePos() {
	return pos;
}
char KernelFile::eof() {
	if (pos == size) return 2;
	else if (pos < size) return 0;
	else return 1;
}
BytesCnt KernelFile::getFileSize() {
	return size;
}
char KernelFile::truncate() {
	BytesCnt end = pos;
	BytesCnt i = pos;
	char* block = new char[ClusterSize];
	char* cleaner = new char[ClusterSize];
	char* index2 = new char[ClusterSize];
	BytesCnt num1 = pos / (numEntry*ClusterSize);
	BytesCnt num2 = (pos % (numEntry*ClusterSize)) / ClusterSize + 1;
	pos += ClusterSize - pos % ClusterSize;
	int* t = (int*)(&index[num1 * 4]);
	kernelFS->partition->readCluster(*t, index2);
	for (int i = 0; i < ClusterSize; i++) cleaner[i] = 0x00;
	while (pos < size) {
		for (int k = num2; k < numEntry; k++) {
			t = (int*)(&index2[k * 4]);
			if (*t == 0) break;
			kernelFS->partition->writeCluster(*t, cleaner);
			*t = 0;
			pos += ClusterSize;
		}
		if (num2 == 0) {
			t = (int*)(&index[num1 * 4]);
			kernelFS->partition->writeCluster(*t, cleaner);
		    *t = 0;
		}
		num2 = 0; num1++;
		t = (int*)(&index[num1 * 4]);
		kernelFS->partition->readCluster(*t, index2);
	}
	pos = size = end;
	return 1;
}

void KernelFile::init(char*fname, char mode, BytesCnt size, int ind1, int ind2, int fileBlock, BytesCnt pos, KernelFS* kernelFS)
{   
	this->fname = fname;
	this->mode = mode;
	this->size = size;
	this->ind1 = ind1;
	this->ind2 = ind2;
	this->fileBlock = fileBlock;
	this->pos = pos;
	this->kernelFS = kernelFS;
	index = new char[ClusterSize];
	kernelFS->partition->readCluster(fileBlock, index);
}

















