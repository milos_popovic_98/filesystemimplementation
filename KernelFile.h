#ifndef _KERNELFILE_H_
#define _KERNELFILE_H_
#include "FS.h"
#include "KernelFS.h"
class KernelFile
{
public:
	KernelFile();
	~KernelFile();
	char write(BytesCnt, char* buffer);
	char write1(BytesCnt cnt, char * buffer);
	char write2(BytesCnt cnt, char * buffer);
	BytesCnt read(BytesCnt, char* buffer);
	char seek(BytesCnt);
	BytesCnt filePos();
	char eof();
	BytesCnt getFileSize();
	char truncate();
	void init(char* fname, char mode, BytesCnt size, int ind1, int ind2, int fileBlock, BytesCnt pos, KernelFS* kernelFS);
private:
	char* fname;
	char mode;
	BytesCnt size;
	int ind1, ind2, fileBlock;
	BytesCnt pos;
	KernelFS* kernelFS;
	char* index = nullptr;
	static int numEntry;
	static BytesCnt MAX;
};
#endif

