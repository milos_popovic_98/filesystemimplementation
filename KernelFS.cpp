#include "KernelFS.h"
#include "KernelFile.h"
int MAX_SEM_COUNT = 1;
const int KernelFS::ENTRY = 0;
const int KernelFS::EXT=8;
const int KernelFS::INDEX=12;
const int KernelFS::SIZE=16;
const int KernelFS::FREE=20;
KernelFS::KernelFS()
{
	numEntry = ClusterSize / 32;
	ghMount = CreateSemaphore(
		NULL,           // default security attributes
		MAX_SEM_COUNT,  // initial count
		MAX_SEM_COUNT,  // maximum count
		NULL);          // unnamed semaphore
	ghFormat = CreateSemaphore(NULL, MAX_SEM_COUNT, MAX_SEM_COUNT, NULL);
	ghMutex= CreateSemaphore(NULL, MAX_SEM_COUNT, MAX_SEM_COUNT, NULL);
	ghBit = CreateSemaphore(NULL, MAX_SEM_COUNT, MAX_SEM_COUNT, NULL);
	blockFlag = false;
	list = new List();
}


KernelFS::~KernelFS()
{
	delete partition;
	CloseHandle(ghMutex);
	CloseHandle(ghMount);
	CloseHandle(ghFormat);
	CloseHandle(ghBit);
}



char KernelFS::mount(Partition * partition)
{
	wait(ghMount);
	this->partition = partition;
	return 1;
	
}
void KernelFS::wait(HANDLE ghSemaphore) {
	WaitForSingleObject(
		ghSemaphore,
		INFINITE);
}
void KernelFS::signal(HANDLE ghSemaphore) {
	ReleaseSemaphore(
		ghSemaphore,  // handle to semaphore
		1,            // increase count by one
		NULL);
}
char KernelFS::unmount()
{
	delete partition;
	signal(ghMount);
	return 1;
}

char KernelFS::format()
{
	wait(ghMutex);
	if (partition == nullptr) {
		signal(ghMutex);  return 0;
	}
	else {
		blockFlag = true;
		if(openFileCnt > 0) {
			signal(ghMutex);
			wait(ghFormat);
		}
		//initializeBitVector();
		ClusterNo num = partition->getNumOfClusters();
		char* buffer = new char[ClusterSize];
		for (unsigned int i = 0; i < ClusterSize; i++) {
			buffer[i] = 0x00;
		}
		buffer[0] = 0xC0;
		partition->writeCluster(0, buffer);
		buffer[0] = 0x00;
		for (unsigned long j = 1; j < num; j++) {
			partition->writeCluster(j, buffer);
		}
		delete[] buffer;
	}
	blockFlag = false;
	signal(ghMutex);
	return 1;
}
void KernelFS::initializeBitVector() {
	ClusterNo num = partition->getNumOfClusters();
	char* buffer = new char[ClusterSize];
	for (unsigned int i = 0; i < ClusterSize; i++) {
		buffer[i] = 0x00;
	}
	buffer[0] = 0xC0;
	partition->writeCluster(0, buffer);
	buffer[0] = 0x00;
	for (unsigned long j = 1; j < num; j++) {
		partition->writeCluster(j, buffer);
	}
	delete [] buffer;
}
void KernelFS::initializeRoot() {
	char* buffer = new char[ClusterSize];
	for (unsigned int i = 0; i < ClusterSize; i++) {
		buffer[i] = 0x00;
	}
	partition->writeCluster(1, buffer);
	delete [] buffer;
}
FileCnt KernelFS::readRootDir() 
{
	wait(ghMutex);
	unsigned long suma = 0;
	char * index1 = new char[ClusterSize];
	partition->readCluster(1, index1);
	for (int i = 0; i < numEntry; i++) {
		if (index1[i * 32] == ' ') {
			suma += *((int*)(&index1[i * 32 + FREE]));
		}
		else if (index1[i * 32] != 0x00) suma++;
	}
	signal(ghMutex);
	delete [] index1;
	return suma;
}


char KernelFS::doesExist(char * fname) 
{
	wait(ghMutex);
	char* index1 = new char[ClusterSize];
	partition->readCluster(1, index1);
	for (int i = 0; i < numEntry; i++) {//prolazi kroz indekse prvog nivoa
		if (index1[i * 32] != ' ' && index1[i * 32] != 0x00) {
			if (equals(&index1[i * 32], fname)) {
				delete[] index1; 
				signal(ghMutex);
				return true;
			}
		}
	}
	char* index2 = new char[ClusterSize];
	for (int i = 0; i < numEntry; i++) {//provera indeksa drugog nivoa
		if (index1[i * 32] == ' ') {
			partition->readCluster(*((int*)&index1[i * 32 + INDEX]), index2);
			for (int j = 0; j < numEntry; j++) {//provera ulaza indeksa2
				if (index2[j*32] != 0x00) {
					if (equals(&index2[j * 32], fname)) {
						delete[] index1; delete[] index2;
						signal(ghMutex);
						return true;
					}
				}
			}

		}
	}
	delete[] index1; delete[] index2;
	signal(ghMutex);
	return false;
}
bool KernelFS::equals(char* buffer, char* fname) const{
	int k;
	for (k = 0; k < 8; k++) {
		if (buffer[k] != fname[k+1]) {
			if (buffer[k] == ' ' && fname[k+1] == '.') break;
			else return false;
		}
	}
	k += 2;
	for (int i = 0; i < 3; i++) {
		if (buffer[EXT + i] != fname[k + i]) {
				if (buffer[EXT + i] == ' ' && fname[k + i] == '\0') return true;
				else return false;
			}
	}
	return true;
}
File * KernelFS::open(char * fname, char mode)
{
	wait(ghMutex);
	if (blockFlag == true) {
		signal(ghMutex);return nullptr;
	}
	char* index1 = new char[ClusterSize];
	char* index2 = new char[ClusterSize];
	int ind1 = -1, ind2 = -1;
	int size, fileBlock;
	File* file;
	if (mode == 'r') {
		if (!findFile(fname, &ind1, &ind2, index1, index2)) {
			signal(ghMutex); delete[] index1; delete[] index2;  return nullptr;
		}
		int* open;
		if (ind2!=-1) {
			open = (int*)(&index2[ind2 * 32+FREE]);
			(*open)++;
			partition->writeCluster(*((int*)(&index1[ind1 * 32])), index2);
		}
		else {
			open = (int*)(&index1[ind1 * 32+FREE]);
			(*open)++;
			partition->writeCluster(1, index1);
		}
		if (*open == 1) {
				SRWLOCK* lock = new SRWLOCK();
				InitializeSRWLock(lock);
				list->add(lock, fname);
				AcquireSRWLockShared(lock);
			}
		else {
			SRWLOCK* lock = list->get(fname);
			signal(ghMutex);
			AcquireSRWLockShared(lock);
			wait(ghMutex);
			if (ind2 != -1) partition->readCluster(*((int*)(&index1[ind1 * 32])), index2);
			else partition->readCluster(1, index1);
		}
		if (ind2 != -1) {
			size = *((int*)(&index2[ind2 * 32 + SIZE]));
			fileBlock = *((int*)(&index2[ind2 * 32 + INDEX]));
		}
		else {
			size = *((int*)(&index1[ind1 * 32 + SIZE]));
			fileBlock = *((int*)(&index1[ind1 * 32 + INDEX]));
		}
		file = new File();
		file->myImpl->init(fname,mode, size, ind1, ind2, fileBlock, 0, this);
		openFileCnt++;
		signal(ghMutex); delete[] index1; delete[] index2;
		return file;
	}
	else if (mode == 'w') {
		if (findFile(fname, &ind1, &ind2, index1, index2)) {
			int* open;
			if (ind2 != -1) {
				open = (int*)(&index2[ind2 * 32 + FREE]);
				(*open)++;
				partition->writeCluster(*((int*)(&index1[ind1 * 32])), index2);
			}
			else {
				open = (int*)(&index1[ind1 * 32 + FREE]);
				(*open)++;
				partition->writeCluster(1, index1);
			}
			if (*open == 1) {
				SRWLOCK* lock = new SRWLOCK();
				InitializeSRWLock(lock);
				list->add(lock, fname);
				AcquireSRWLockExclusive(lock);
			}
			else {
				SRWLOCK* lock = list->get(fname);
				signal(ghMutex);
				AcquireSRWLockExclusive(lock);
				wait(ghMutex);
				if (ind2 != -1) partition->readCluster(*((int*)(&index1[ind1 * 32])), index2);
				else partition->readCluster(1, index1);
			}

			char* bitVector = new char[ClusterSize];
			if (ind2!=-1) fileBlock = *((int*)(&index2[ind2 * 32 + INDEX]));
			else fileBlock = *((int*)(&index1[ind1 * 32 + INDEX]));
			partition->readCluster(0, bitVector);
			emptyFile(fileBlock, bitVector);
			if (ind2!=-1) {
			int *t = (int*)(&index2[ind2 * 32 + SIZE]);
			*t = 0;
			partition->writeCluster(*((int*)(&index1[ind1 * 32])), index2);
		    }
			else {
				int *t = (int*)(&index1[ind1 * 32 + SIZE]);
				*t = 0;
				partition->writeCluster(1, index1);
			}
			File* file = new File();
			file->myImpl->init(fname,mode, 0, ind1, ind2, fileBlock, 0, this);
			openFileCnt++;
			partition->writeCluster(0, bitVector);
			signal(ghMutex); delete[] index1; delete[] index2; delete[] bitVector;
			return file;
		}
		else {
			SRWLOCK* lock=new SRWLOCK();
			InitializeSRWLock(lock);
			list->add(lock, fname);
			AcquireSRWLockExclusive(lock);
			createFile(fname,&ind1,&ind2,&fileBlock);
			File* file = new File();
			file->myImpl->init(fname,mode, 0, ind1, ind2, fileBlock, 0, this);
			openFileCnt++;
			signal(ghMutex);
			delete[] index1; delete[] index2;
			return file;
		}
	}
	else if (mode == 'a') {
		if (!findFile(fname, &ind1, &ind2, index1, index2)) {
			delete[] index1; delete[] index2; return nullptr;
		}
		int* open;
		if (ind2 != -1) {
			open = (int*)(&index2[ind2 * 32 + FREE]);
			(*open)++;
			partition->writeCluster(*((int*)(&index1[ind1 * 32])), index2);
		}
		else {
			open = (int*)(&index1[ind1 * 32 + FREE]);
			(*open)++;
			partition->writeCluster(1, index1);
		}
		if (*open == 1) {
			SRWLOCK* lock = new SRWLOCK();
			InitializeSRWLock(lock);
			list->add(lock, fname);
			AcquireSRWLockExclusive(lock);
		}
		else {
			SRWLOCK* lock = list->get(fname);
			signal(ghMutex);
			AcquireSRWLockExclusive(lock);
			wait(ghMutex);
			if (ind2 != -1) partition->readCluster(*((int*)(&index1[ind1 * 32])), index2);
			else partition->readCluster(1, index1);
		}
		if (ind2!=-1) {
			fileBlock = *((int*)(&index2[ind2 * 32 + INDEX]));
			size = *((int*)(&index2[ind2 * 32 + SIZE]));
		}
		else {
			fileBlock = *((int*)(&index1[ind1 * 32 + INDEX]));
			size = *((int*)(&index1[ind1 * 32 + SIZE]));
		}
		File* file = new File();
		file->myImpl->init(fname,mode, size, ind1, ind2, fileBlock, size, this);
		openFileCnt++;
		signal(ghMutex); delete[] index1; delete[] index2;
		return file;
	}
	return nullptr;
}

void KernelFS::createFile(char* fname,int* ind1,int*ind2,int* fileBlock) 	{

		char* bitVector = new char[ClusterSize];
		//partition->readCluster(0, bitVector);
		int index = 0;
		freeBit(bitVector, &index);//pronalazi slobodan blok za indeks prvog nivoa fajla
		*fileBlock = index;
		char* root = new char[ClusterSize];
		int index2 = -1;
		int entry = -1;
		partition->readCluster(1, root);
		for (int i = 0; i < numEntry; i++) {
			if (root[i * 32] == 0x00) {//trazimo slobodan ulaz u indeksu 1. nivoa
				fillEntry(&root[i * 32], fname, index);
				int* tmp = (int *)(&root[i * 32 + SIZE]);
				*tmp = 0;//velicina fajla
				//partition->writeCluster(0, bitVector);
				partition->writeCluster(1, root);
				*ind1 = i;
				delete[] bitVector; delete[] root;
				return;
			}
			else if (root[i * 32] == ' ' && (*((int*)&root[i * 32 + FREE])) < numEntry && index2 == -1) {
				index2 = *((int*)root[i * 32 + INDEX]);
				entry = i;
			}
		}
		if (index2 != -1) {//postoji indeks 2. nivoa u kome ima slobodnih ulaza
			int* t = (int*)(&root[entry * 32 + FREE]);
			(*t)++;//inkrementiranje broja fajlova
			//partition->writeCluster(0, bitVector);
			partition->writeCluster(1, root);
			partition->readCluster(index2, root);//ucitavanje indeksa drugog nivoa
			for (int i = 0; i < numEntry; i++) {
				if (root[i * 32] == 0x00) {//trazenje slobodnog ulaza u indeksu 2. nivoa
					fillEntry(&(root[i * 32]), fname, index);
					int* tmp = (int*)(&root[i * 32 + SIZE]);
					*tmp = 0;//velicina fajla
					partition->writeCluster(index2, root);
					//partition->writeCluster(0, bitVector);
					*ind1 = index2; *ind2 = i;
					delete[] bitVector; delete[] root;
					return;
					}
				}
			}
			else {// ne postoji indeks 2. nivoa sa slobodnim ulazom
				for (int i = 0; i < numEntry; i++) {
					if (root[i * 32] != ' ') {//pravljenje indeksa drugog nivoa
						freeBit(bitVector, &index2);//trazenje slobodnog bloka
						char* indd2 = new char[64];
						partition->readCluster(index2, indd2);
						for (int j = 0; j < 32; j++) {//prepisivanje ulaza
							indd2[j] = root[i * 32 + j];
						}
						fillEntry(&indd2[32], fname, index);
						root[i * 32] = ' ';//oznacava ulaz drugog nivoa
						int* t = (int*)(&(root[i * 32 + FREE]));
						*t = 2;//broj zauzetih ulaza drugog nivoa
						t = (int*)(&indd2[32 + SIZE]);
						*t = 0;
						//partition->writeCluster(0, bitVector);
						partition->writeCluster(1, root);
						partition->writeCluster(index2, indd2);
						*ind1 = i; *ind2 = index2;
						delete[] bitVector; delete[] root; delete[] indd2;
						return;
					}
				}
			}
}
void KernelFS::freeBit(char* bitVector, int* index) {
	wait(ghBit);
	partition->readCluster(0, bitVector);
	for (int i = 0; i < ClusterSize; i++) {
			if (bitVector[i] != 0xff) {
				for (int j = 7; j >= 0; j--) {
					if ((bitVector[i] & (1 << j)) == 0x00) {//slobodan ulaz
						bitVector[i] |= 1 << j;
						*index = i * 8 + (8 - j - 1);
						partition->writeCluster(0, bitVector);
						signal(ghBit);
						return;
					}
				}
			}
		}
	partition->writeCluster(0, bitVector);
	signal(ghBit);
}
void KernelFS::fillEntry(char* dst, char* src, int index)
	{
		int i = 0;
		while (i < 8 && src[i+1] != '.') {
			dst[i] = src[i+1];
			i++;
		}
		int ext = i + 2;
		while (i < 8) {
			dst[i] = ' ';
			i++;
		}
		i = 0;
		while (i < 3 && src[ext + i] != '\0') {
			dst[EXT + i] = src[ext + i];
			i++;
		}
		while (i < 3) {
			dst[EXT + i] = ' ';
			i++;
		}
		dst[EXT + i] = 0x00;
		int * t = (int*)(&dst[INDEX]);
		*t = index;
		t = (int*)(&(dst[FREE]));
		*t = 1;//otvoren fajl
}
		

bool KernelFS::findFile(char* fname,int* ind1,int* ind2,char* index1,char* index2) {
	int* open = nullptr;
	partition->readCluster(1, index1);
	for (int i = 0; i < numEntry; i++) {//prolazi kroz indekse prvog nivoa
		if ((index1[i * 32] != ' ') && (index1[i * 32] != 0x00)) {
			if (equals(&index1[i * 32], fname)) {
					*ind1 = i; return true; 
			}
		}
	}
	for (int i = 0; i < numEntry; i++) {//provera indeksa drugog nivoa
		if (index1[i * 32] == ' ') {
			partition->readCluster(*((int*)&index1[i * 32 + INDEX]), index2);
			for (int j = 0; j < numEntry; j++) {//provera ulaza indeksa2
				if (index2[j * 32] != 0x00) {
					if (equals(&index2[j * 32], fname)) {
							*ind1 = i; *ind2 = j; return true;
					}
				}
			}

		}
	}
	return false;
}
void KernelFS::emptyFile(int fileBlock, char* bitVector) {
	char* index1 = new char[ClusterSize];
	char* index2 = new char[ClusterSize];
	char* cleaner = new char[ClusterSize];
	for (unsigned i = 0; i < ClusterSize; i++) cleaner[i] = 0x00;
	int* entry1, *entry2;
	partition->readCluster(fileBlock, index1);
	int num = 8 * numEntry;
	for (int i = 0; i < num; i++) {//prolazak kroz indeks 1. nivoa
		if (index1[i] != 0x00) {
			entry1 = (int*)(&index1[i * 4]);
			partition->readCluster(*entry1, index2);
			for (int j = 0; j < num; j++) {//prolazak kroz indeks 2. nivoa
				if (index2[j * 4] != 0x00) {
					entry2 = (int*)(&index2[j * 4]);
					partition->writeCluster(*entry2, cleaner);
					resetBit(bitVector, *entry2);
				}
			}
			partition->writeCluster(*entry1, cleaner);//praznjenje indeksa 2. nivoa
			resetBit(bitVector, *entry1);
		}
	}
	partition->writeCluster(fileBlock, cleaner);//praznjenje indeksa 1. nivoa
	delete[] cleaner; delete[] index1; delete[] index2;
}
void KernelFS::resetBit(char* bitVector, int entry) {
	int blk = entry / 8;
	char mask = ~(1 << (7 - entry % 8));
	bitVector[blk] &= mask;
}
char KernelFS::deleteFile(char * fname){
	wait(ghMutex);
	char* bitVector = new char[ClusterSize];
	char* index1 = new char[ClusterSize];
	char* index2 = new char[ClusterSize];
	int* ind1 = nullptr, *ind2 = nullptr;
	if (!findFile(fname, ind1, ind2, index1, index2)) {
		signal(ghMutex); delete[] bitVector; delete[] index1; delete[] index2;
		return 0;
	}
	int fileBlock;
	int* isOpen = nullptr;
	if (ind2) {
		isOpen = (int*)(&index2[(*ind2) * 32 + FREE]);
		if (*isOpen > 0) {
			signal(ghMutex); delete[] bitVector; delete[] index1; delete[] index2;
			return 0;
		}
		fileBlock = *((int*)(&index2[(*ind2) * 32 + INDEX]));
	}
	else {
		isOpen = (int*)(&index1[(*ind1) * 32 + FREE]);
		if (*isOpen > 0) {
			signal(ghMutex); delete[] bitVector; delete[] index1; delete[] index2;
			return 0;
		}
		fileBlock = *((int*)(&index1[(*ind1) * 32 + INDEX]));
   	}
	partition->readCluster(0, bitVector);
	emptyFile(fileBlock,bitVector);
	resetBit(bitVector, fileBlock);
	if (ind2) {
		index2[(*ind2) * 32] = 0x00;
		int* t = (int*)(&index1[(*ind1) * 32+FREE]);
		(*t)--;
		if ((*t) == 0) {//nema vise fajlova u ulazu drugog nivoa
			char* cleaner = new char[ClusterSize];
			for (unsigned i = 0; i < ClusterSize; i++) cleaner[i] = 0x00;
			partition->writeCluster(*((int*)(&index1[(*ind1) * 32 + INDEX])), cleaner);
			resetBit(bitVector, *((int*)(&index1[(*ind1) * 32 + INDEX])));//oslobadjanje indeksa 2. nivoa direktorijuma
			index1[(*ind1) * 32] = 0x00;
		}
	}
	else {
		index1[(*ind1) * 32] = 0x00;//oslobadjanje ulaza prvog nivoa
	}
	partition->writeCluster(0, bitVector);
	partition->writeCluster(1, index1);
	if (ind2) partition->writeCluster(*((int*)&index1[(*ind2) * 32 + INDEX]), index2);
	signal(ghMutex); delete[] bitVector; delete[] index1; delete[] index2;
	return 1;
}

void KernelFS::close(char* fname,char mode, int ind1, int ind2,int fileBlock, char * index, BytesCnt size)
{   
	int* t = nullptr;
	char* index1 = new char[ClusterSize];
	wait(ghMutex);
	partition->readCluster(1, index1);
	if (ind2 != -1) {
		char* index2 = new char[ClusterSize];
		int *tmp = (int*)(&index1[ind1 * 32 + INDEX]);
		partition->readCluster(*tmp, index2);
		t = (int*)(&index2[ind2 * 32 + FREE]);
		(*t)--;
		if (*t == 0) list->remove(fname);
		else {
			SRWLOCK* lock = list->get(fname);
			if (mode == 'r') ReleaseSRWLockShared(lock);
			else ReleaseSRWLockExclusive(lock);
		}
		t = (int*)(&index2[ind2 * 32 + SIZE]);
		*t = size;
		partition->writeCluster(*tmp, index2);
	}
	else {
		t = (int*)(&index1[ind1 * 32 + FREE]);
		(*t)--;
		if (*t == 0)
			list->remove(fname);
		else {
			SRWLOCK* lock = list->get(fname);
			if (mode == 'r') ReleaseSRWLockShared(lock);
			else ReleaseSRWLockExclusive(lock);
		}
		t = (int*)(&index1[ind1 * 32 + SIZE]);
		*t = size;
		partition->writeCluster(1, index1);
	}
	openFileCnt--;
	partition->writeCluster(fileBlock, index);
	if (openFileCnt == 0 && blockFlag) signal(ghFormat);
	else signal(ghMutex);              
}












