#ifndef _LIST_H_
#define _LIST_H_
#include "Windows.h"
class List
{
public:
	List();
	~List();
	void add(SRWLOCK* p, char* n);
	SRWLOCK* get(char* n);
	void remove(char* n);
	bool equals(char* n1, char* n2);
private:
	struct Elem {
		SRWLOCK* pod;
		char* name;
		Elem* sled;
		Elem(SRWLOCK* p,char* n):pod(p),name(n){
			sled = nullptr;
		}
	};
	Elem* front, *rear;
};
#endif

