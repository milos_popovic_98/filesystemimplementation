#ifndef _KERNELFS_H_
#define _KERNELFS_H_
#include <Windows.h>
#include <map>
#include "part.h"
#include "file.h"
#include "List.h"
class KernelFS
{
public:
	KernelFS();
	~KernelFS();
	char mount(Partition* partition);//montira particiju
				// vraca 0 u slucaju neuspeha ili 1 u slucaju uspeha
	char unmount();//demontira particiju
				// vraca 0 u slucaju neuspeha ili 1 u slucaju uspeha
	char format();//formatira particiju
				// vraca 0 u slucaju neuspeha ili 1 u slucaju uspeha
	FileCnt readRootDir() ;
	// vraca -1 u slucaju neuspeha ili broj fajlova u slucaju uspeha
	char doesExist(char* fname) ;//argument je naziv fajla sa apsolutnom putanjom

	File* open(char* fname, char mode);
	void createFile(char * fname, int * ind1, int * ind2, int * fileBlock);
	bool findFile(char * fname, int * ind1, int * ind2, char * index1, char * index2);
	void emptyFile(int fileBlock, char * bitVector);
	void resetBit(char * bitVector, int entry);
	char deleteFile(char* fname);
	void close(char* fname,char mode, int ind1, int ind2, int fileBlock, char * index, BytesCnt size);
private:
	friend class KernelFile;
	Partition* partition;
	List* list;
	int numEntry;
	HANDLE ghMount;
	HANDLE ghFormat;
	HANDLE ghMutex;
	HANDLE ghBit;
	int openFileCnt;
	bool blockFlag;
	void initializeBitVector();
	void initializeRoot();

	inline void wait(HANDLE ghSemaphore);
	inline void signal(HANDLE ghSemaphore);
	bool equals(char* buffer, char* fname) const;
	void freeBit(char* bitVector, int* index);
	void fillEntry(char* dst, char* src, int index);

	static const int ENTRY;
	static const int EXT;
	static const int INDEX;
	static const int SIZE;
	static const int FREE;
};
#endif






















