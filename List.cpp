#include "List.h"



List::List()
{
	front = rear = nullptr;
}
void List::add(SRWLOCK* p, char* n) {
	Elem* t = new Elem(p, n);
	if (!front) front = t;
	else rear->sled = t;
	rear = t;
}
SRWLOCK* List::get(char* n) {
	Elem* t = front;
	while (t) {
		if (equals(t->name, n)) return t->pod;
		t = t->sled;
	}
	return nullptr;
}
bool List::equals(char* n1, char*n2) {
	int i = 0;
	while (n1[i] != '\0' && n2[i] != '\0' && n1[i] == n2[i])i++;
	if (n1[i] == '\0' && n2[i] == '\0')return true;
	else return false;
}
void List::remove(char* n) {
	Elem* pret = nullptr;
	Elem* t = front;
	while (t) {
		if (equals(t->name, n)) break;
		pret = t; t = t->sled;
	}
	if (!t) return;
	if (!pret) front = t->sled;
	else {
		pret->sled = t->sled;
		if (t == rear) rear = pret;
	}
	delete t;
}

List::~List()
{
	while (front != nullptr) {
		Elem* t = front;
		front = front->sled;
		delete t;
	}
	rear = nullptr;
}




















